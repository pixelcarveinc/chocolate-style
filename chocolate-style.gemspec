$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "chocolate/style/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name          = "chocolate-style"
  s.version       = Chocolate::Style::VERSION
  s.authors       = ['Pixelcarve Inc.']
  s.email         = ['operations@pixelcarve.com']
  s.homepage      = 'http://pixelcarve.com/'
  s.summary       = 'Chocolate styling library for Rails'
  s.description   = 'Chocolate styling library for Rails, basic building block for Chocolate CMS'

  s.files = Dir["{app,config,db,lib,vendor}/**/*", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0"
  s.add_dependency "breadcrumbs_on_rails"
  s.add_dependency "turbolinks"
  s.add_dependency "jquery-rails"
  s.add_dependency "ckeditor_rails"
  s.add_dependency "pxcv-rails"

  s.add_development_dependency "sqlite3"
end
