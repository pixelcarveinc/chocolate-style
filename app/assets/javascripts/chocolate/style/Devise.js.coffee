#= require chocolate/style/namespace


class window.chocolate.style.Devise
  
  start: ->
    @checkboxes = $('.checkbox').map ->
      new chocolate.style.views.forms.elements.PseudoCheckbox el: $(@)
