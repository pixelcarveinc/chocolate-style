#= require  chocolate/style/namespace
#= require  chocolate/style/views/forms/FullForm
#= require  chocolate/style/views/Navigation


# Application controlling CMS JavaScript views.  Through sub-views, manages
# navigation, forms, listings, activity and lightbox links.
#
# Events:
#
# configure (after construction)
# prestart (start called but not yet started)
# start (start called, before first resize)
# resize (window resize)
# precleanup (clean up called but not yet cleaned up)
# cleanup (before end of clean up)
class window.chocolate.style.Cms

  _.extend @prototype, Backbone.Events

  constructor: (@options = {}) ->
    # fix for back button in disabling JavaScript issue
    # http://stackoverflow.com/questions/17029399/clicking-back-in-the-browser-disables-my-javascript-code-if-im-using-turbolin
    Turbolinks.pagesCached(0);

    @options.paths ||= {}
    @configurePaths()
    @configureCKEditor()
    @trigger 'configure', this

  start: ->
    @trigger 'prestart', this

    @navigation = new chocolate.style.views.Navigation el: '#navigation'

    @forms = $('form.full').map (i, el) =>
      o = _.extend {}, @options, el: el
      pxcv.buildClass $(el).data('formclass'), chocolate.style.views.forms.FullForm, o
    .get()

    if $('#listing').length
      o = _.extend {}, @options, el: '#listing'
      @listing = pxcv.buildClass $('#listing').data('listingclass'), chocolate.style.views.Listing, o

    if $('#activity').length
      @activity = new chocolate.style.views.Activity el: '#activity'

    lightbox.init()

    @trigger 'start', this

    # resize handler for global layout changes
    $(window).on 'resize', _.debounce(@_resize, 100)
    @_resize()

    $(document).on 'page:before-change', @_beforeChange

  # Cleaning up JavaScript views and callbacks is important because Turbolinks
  # reloads pages through AJAX.
  cleanUp: ->
    @trigger 'precleanup', this
    form.remove() for form in @forms
    @listing?.remove()
    @activity?.remove()

    $(window).off   'resize'
    $(document).off 'page:before-change'

    lightbox.destroy()

    @navigation = @forms = @listing = null
    @trigger 'cleanup', this

  _resize: =>
    form.resize() for form in @forms
    @listing?.resize()
    @activity?.resize()
    @trigger 'resize', this

  # Page change from Turbolinks, tear down JavaScript application as it will be
  # re-created when DOM is loaded.
  _beforeChange: =>
    @cleanUp()
    delete window.cms

  # Magically setup path helpers if they exists.
  #
  # For example, if paths are specified under the frame namespace this will look
  # for setFramePaths on the JST object and pass the paths from options to it.
  configurePaths: ->
    for own name, paths of @options.paths
      setFunc = "set#{name.ucfirst()}Paths"
      JST[setFunc](paths) if JST[setFunc]?

  configureCKEditor: ->
    if @options.paths.selectFolders?
      selectPath = @options.paths.selectFolders + '?mode=files'

    _.extend CKEDITOR.config,
      allowedContent: true, # filtering is handled by Active Support in the backend
      contentsCss: null,
      toolbarGroups: [
          { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
          { name: 'paragraph',   groups: [ 'list', 'indent', 'align' ] },
          { name: 'styles' },
          { name: 'links' },
          { name: 'colors' },
          { name: 'insert' },
          { name: 'document', groups: [ 'mode' ] }
      ],
      removeButtons:    'Cut,Copy,Paste,Undo,Redo,Anchor,Subscript,Superscript,Styles,Font,BGColor,Flash,Smiley,PageBreak,Iframe,Save,NewPage,Preview,Print',
      removeDialogTabs: 'link:advanced'
      filebrowserBrowseUrl: selectPath
