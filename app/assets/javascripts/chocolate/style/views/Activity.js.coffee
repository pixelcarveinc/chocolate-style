#= require  chocolate/style/namespace


class window.chocolate.style.views.Activity extends Backbone.View
  
  initialize: ->
    _.extend @, chocolate.style.views.utils.SlyResizeMixin
    
    @$frame     = @$ '.frame'
    @$scrollbar = @$ '.scrollbar'
    @$content   = @$ 'ul.content'
    
    @sly = new Sly @$frame,
      scrollBy:      100
      speed:         300
      scrollBar:     @$scrollbar
      dragHandle:    1
      dynamicHandle: 1
      clickBar:      1
    @sly.init()
    
  resize: ->
    @resizeSly()
    
  remove: ->
    @cleanUp()
    super

  cleanUp: ->
    @sly.destroy()
  