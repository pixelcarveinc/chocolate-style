#= require  chocolate/style/namespace


# Fullscreen form
class window.chocolate.style.views.forms.FullForm extends Backbone.View

  events:
    "change #page_type": '_setPageType'

  initialize: ->
    # provides resize method for sly scroller
    _.extend @, chocolate.style.views.utils.SlyResizeMixin

    # store some stuff required by sly mixin
    @$frame     = @$ '.frame'
    @$scrollbar = @$ '.scrollbar'
    @$content   = @$ 'fieldset.fields'
    @$controls  = @$ 'fieldset#controls'

    # init file selects
    @files = @$('.file-field, .image-field').map ->
      new chocolate.style.views.forms.elements.FileSelect el: $(@)
    .get()

    # init yes/no toggle buttons
    @yesnos = @$('.radio-toggle').map ->
      new chocolate.style.views.forms.elements.RadioToggle el: $(@)
    .get()

    # init date/time pickers
    @datetime = @$('.datetimepicker').datetimepicker
      timeFormat: "h:mm tt"
      dateFormat: 'yy-mm-dd'

    # init sly scroller
    @sly = new Sly @$frame,
      scrollBy:      100
      speed:         300
      scrollBar:     @$scrollbar
      dragHandle:    1
      dynamicHandle: 1
      clickBar:      1
    @sly.init()

    # needed for ckeditor callback
    _this = @
    @editors = @$('textarea.ck').ckeditor ->
      # context is editor instance, can't get from args
      @on 'resize', _this.resize
      _this.resize()

  resize: =>
    @resizeSly()

  remove: ->
    @cleanUp()
    super

  cleanUp: ->
    @sly.destroy()
    for ed in @editors
      $(ed).ckeditorGet().destroy()

  _setPageType: (item) ->
    #window.location.href = window.location.pathname + '/' + $(item.target).find(':selected').val()

