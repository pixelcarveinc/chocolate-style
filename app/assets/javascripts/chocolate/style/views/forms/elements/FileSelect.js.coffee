#= require chocolate/style/namespace


class window.chocolate.style.views.forms.elements.FileSelect extends Backbone.View
  
  selectedTemplate: JST['chocolate/style/forms/elements/file_selected']
  
  events:
    'change input': '_fileSelected'
    'click .remove': '_fileRemoved' 
        
  initialize: ->
    @$input    = @$ 'input'
    @$selected = @$ '.selected'
    
  _fileSelected: ->
    @$selected.html @selectedTemplate
      file: @$input[0].files[0]

  _fileRemoved: (e) ->
    e.preventDefault()
    @$selected.html ''
    # somewhat hacky way to clear file input value but it works
    @$input.attr('type', 'text').attr('type', 'file')
