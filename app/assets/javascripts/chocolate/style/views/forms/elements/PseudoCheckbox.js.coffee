#= require  chocolate/style/namespace


class window.chocolate.style.views.forms.elements.PseudoCheckbox extends Backbone.View
  
  events:
    'click button': '_click'
    
  initialize: ->
    @$input  = @$ 'input'
    @$button = @$ 'button'
    @checked = @$button.hasClass 'checked'
    
    @checkedValue   = @$input.data  'checked-value'
    @uncheckedValue = @$input.data 'unchecked-value'
    
    # try treating as numeric to fix comparison problems between data/val conversions
    val = parseInt @$input.val()
    val = @$input.val() if isNaN val
    
    if val is @checkedValue then @check() else @uncheck()
    
  toggle: ->
    if @checked then @uncheck() else @check()
    @trigger 'toggle', @, @checked
    
  # 3rd state where checkbox is not selected, but shows that one or more things area
  partial: ->
    @checked = false
    @$input.val '0'
    @$button
      .removeClass('checked')
      .addClass('partial')
    @trigger 'partial', @, false
    
  check: ->
    @checked =           true
    @value   =           @checkedValue
    @$input.val          @checkedValue
    @$button.addClass    'checked'
    @$button.removeClass 'partial'
    @trigger 'check', @, @checked
    
  uncheck: ->
    @checked =           false
    @value   =           @uncheckedValue
    @$input.val          @uncheckedValue
    @$button.removeClass 'checked'
    @$button.removeClass 'partial'
    @trigger 'uncheck', @, @checked
    
  _click: (e) ->
    e.preventDefault()
    @toggle()
