#= require  chocolate/style/namespace


# Control for psuedo slide toggle.
class window.chocolate.style.views.forms.elements.SlideToggle extends Backbone.View
  
  events:
    'click': '_click'
    
  initialize: ->
    @$input  = @$ 'input'
    @$slide  = @$ '.slide'
    @$on     = @$ '.on-state'
    @$off    = @$ '.off-state'
    
    @checkedValue   = @$on.data  'value'
    @uncheckedValue = @$off.data 'value'
    
    # try treating as numeric to fix comparison problems between data/val conversions
    val = parseInt @$input.val()
    val = @$input.val() if isNaN val
    
    if val is @checkedValue then @check() else @uncheck()
    
  toggle: ->
    if @checked then @uncheck() else @check()
    @trigger 'toggle', @, @checked
    
  check: ->
    @checked = true
    @$slide.removeClass('off').addClass('on')
    @$input.val @checkedValue
    
  uncheck: ->
    @checked = false
    @$slide.removeClass('on').addClass('off')
    @$input.val @uncheckedValue
    
  _click: ->
    @toggle()
    
