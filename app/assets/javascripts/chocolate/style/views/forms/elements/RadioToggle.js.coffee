#= require  chocolate/style/namespace


# Control for psuedo radio toggle.
class window.chocolate.style.views.forms.elements.RadioToggle extends Backbone.View

  events:
    'click a': '_click'
  
  initialize: ->
    @$input  = @$ 'input'
    @$on     = @$ 'a.on'
    @$off    = @$ 'a.off'
    
    @checkedValue   = @$on.data  'value'
    @uncheckedValue = @$off.data 'value'
    
    # try treating as numeric to fix comparison problems between data/val conversions
    val = parseInt @$input.val()
    val = @$input.val() if isNaN val
    
    if val is @checkedValue then @check() else @uncheck()
    
  toggle: ->
    if @checked then @uncheck() else @check()
    @trigger 'toggle', @, @checked
    
  check: ->
    @checked =        true
    @$on.addClass     'checked'
    @$off.removeClass 'checked'
    @$input.val       @checkedValue
    
  uncheck: ->
    @checked =        false
    @$on.removeClass  'checked'
    @$off.addClass    'checked'
    @$input.val       @uncheckedValue
    
  _click: (e) ->
    checked = e.currentTarget is @$on[0]
    @toggle() if checked isnt @checked
      
