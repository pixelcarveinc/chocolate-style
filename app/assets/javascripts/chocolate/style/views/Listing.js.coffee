#= require  chocolate/style/namespace
#= require  chocolate/style/views/forms/elements/PseudoCheckbox

PseudoCheckbox = chocolate.style.views.forms.elements.PseudoCheckbox


class chocolate.style.views.Listing extends Backbone.View
  
  events:
    'click #controls .delete': '_delete'
    
  deleteTemplate: JST['chocolate/style/listing/delete']
  
  initialize: ->
    _.extend @, chocolate.style.views.utils.SlyResizeMixin
    
    @$frame = @$ '.frame'
    @$scrollbar = @$ '.scrollbar'
    @$headings = @$ 'table.headings'
    @$content = @$ 'table.content'
    @$rows = @$content.find 'tr'
    
    @sly = new Sly @$frame,
      scrollBy:      100
      speed:         300
      scrollBar:     @$scrollbar
      dragHandle:    1
      dynamicHandle: 1
      clickBar:      1
    @sly.init()
    
    @initModel()
    @initSelection()
    @initPublished()
    @initPosition()
	
  resize: ->
    @resizeSly()
    
  remove: ->
    @cleanUp()
    super

  cleanUp: ->
    @sly.destroy()
    _.each @checkboxes, (cb) =>
      @stopListening cb
      cb.remove()
    @stopListening @selectAll
    @selectAll.remove()
  
  # Initialize models and a collection if a listingmodel data attribute is
  # specified.
  initModel: ->
    klass  = @$el.data 'listingmodel'
    return if pxcv.isBlank klass
    
    models = @$rows.map (i, e) ->
      pxcv.buildClass klass, undefined, $(e).data('model')
    .get()
    
    @collection = new chocolate.style.models.ListCollection models
    
    
  # Selection
    
  initSelection: ->
    @checkboxes = @$rows.find('.select > .checkbox').map (i, el) =>
      cb = new PseudoCheckbox el: $(el)
      @listenTo cb, 'toggle', @_toggleOne
      cb
      
    @selectAll = new PseudoCheckbox el: @$headings.find('.select > .checkbox')
    @listenTo @selectAll, 'toggle', @_toggleAll
    
  _toggleAll: (cb, state) ->
    if state is true
      checkbox.check() for checkbox in @checkboxes
    else
      checkbox.uncheck() for checkbox in @checkboxes
    
  _toggleOne: ->
    if _.every(@checkboxes, (cb) -> cb.checked)
      @selectAll.check()
    else if _.every(@checkboxes, (cb) -> not cb.checked)
      @selectAll.uncheck()
    else
      @selectAll.partial()


  # Multi-delete

  _delete: (e) ->
    e.preventDefault()
    
    targets = _.pluck _.select(@checkboxes, (cb) -> cb.checked), 'value'
    return unless targets.length > 0
        
    @_deleteWarning @multiDelete, targets
        
  # Pop up dialog asking for delete confirmation, calls callback on confirm and
  # passes additional arguments.
  _deleteWarning: (callback) ->
    callback = _.bind callback, @
    args     = _.toArray(arguments).slice(1)
    
    $message = $(@deleteTemplate()).dialog _.extend {}, pxcv.views.dialogs.DefaultEase,
      resizable: false
      width:     370
      height:    250
      modal:     true
      buttons:
        Yes: =>
          callback args...
          $message.dialog 'close'
        Cancel: ->
          $message.dialog 'close'
      close: ->
        $message.dialog 'destroy'
    
  multiDelete: (targets) ->
    # dynamically build a form for submitting items to delete
    # could be done using Backbone.Model, but this method will utilize
    # turbolinks and rails for UI rendering
    form = $('<form></form>')
      .attr('method', 'post')
      
    # CSRF token required for forgery protection
    input = $('<input/>')
      .attr('type', 'hidden')
      .attr('name', 'authenticity_token')
      .attr('value', $.csrfToken())
    form.append input
      
    # tell rails to treat request as DELETE
    input = $('<input/>')
      .attr('type', 'hidden')
      .attr('name', '_method')
      .attr('value', 'delete')
    form.append input
      
    # array input for all id values
    for id in targets
      input = $('<input/>')
        .attr('type', 'hidden')
        .attr('name', 'id[]')
        .attr('value', id)
      form.append input
      
    $('body').append(form)
    form.submit()
      
      
  # Publishing
    
  # Initialize published slide toggles if applicable.
  initPublished: ->
    @toggles = @$rows.find('td.published > .slide-toggle').map (i, e) =>
      t = new chocolate.style.views.forms.elements.SlideToggle el: $(e)
      @listenTo t, 'toggle', @togglePublished
      t
    .get()

  togglePublished: (t, c) ->
    @collection.get(t.$el.closest('tr').data('id'))
      .set('published', c)
      .save()
  
  
  # Positioning
  
  # Initialize row drag & drop positioning if applicable.
  initPosition: ->
    return unless @hasPosition()

    @$rows.draggable
      cursorAt:
        top: 5
        left: -20
      appendTo: @$el
      handle: '> .position > .handle'
      cancel: 'a, button'
      opacity: 0.8
      helper: 'clone'
      scroll: false
      zIndex: 100
      refreshPositions: true
      
    @$rows.droppable
      accept: 'tr'
      tolerance: 'pointer'
      activeClass: 'drop-active'
      hoverClass: 'drop-hover'
      greedy: true
      drop: @_changePosition

  # Whether list has manual positioning (acts_as_list).
  hasPosition: -> @$('td.position').length > 0

  _changePosition: (e, ui) =>
    $beingDropped = $ ui.draggable
    $receivingDrop = $ e.target
    
    beingDropped = @collection.get $beingDropped.data('id')
    receivingDrop = @collection.get $receivingDrop.data('id')
    
    @collection.changePosition beingDropped, receivingDrop.get('position')
    @reorderRows()
    
  # Update row UI based on current position values in models.
  reorderRows: ->
    @$content.append _.sortBy @$rows.detach(), (r) ->
      $r = $ r
      p = parseInt @collection.get($r.data('id')).get 'position'
      $r.find('.value').text(p + 1) 
      p
    , @
