#= require  chocolate/style/namespace


chocolate.style.views.utils.SlyResizeMixin =

  # Requires @$el, @$frame, @$scrollbar and @$content to be set.
  resizeSly: ->

    @$el.height window.innerHeight - @$el.offset().top

    iH = _.reduce @$frame.siblings().not('.scrollbar'), ((m, el) -> m + $(el).outerHeight(true)), 0
    @$frame.height window.innerHeight - @$el.offset().top - iH - @$frame.cssNumber('margin-top') - @$frame.cssNumber('margin-bottom')


    if @$frame.height() >= @$content.height()
      @$scrollbar.hide()
    else
      @$scrollbar.show()

    @$scrollbar.height @$frame.height()
    @sly.reload()
