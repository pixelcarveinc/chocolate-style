# common namespace for holding this project's classes
window.chocolate ||= {}
window.chocolate.style =
  models: {}
  views:
    forms:
      elements:         {}
      displayLayout:    {}
    utils:              {}
