#= require  chocolate/style/namespace


# Extends collection to add methods for working with a model using acts_as_list.
class chocolate.style.models.ListCollection extends Backbone.Collection
  
  # collection is ordered by position field
  comparator: (m) -> parseInt m.get 'position'
  
  # Make Space for target's order in collection by shifting existing models out
  # of the way
  #
  # @param {Model} target
  # @param {int} position
  # @returns {this}
  changePosition: (target, position) ->
    oldPosition = parseInt target.get 'position'
    newPosition = parseInt position
    
    # make space for model's new position
    for m in @models
      p = parseInt m.get 'position'
      
      if newPosition < oldPosition
        # moving up
        m.set('position', p + 1) if p >= newPosition and p < oldPosition
      else
        # moving down
        m.set('position', p - 1) if p > oldPosition and p <= newPosition
        
    # set target model's position and save
    target.save position: position
    
    # sort collection
    @sort()
  
    @