require "chocolate/style/version"
require "chocolate/style/engine"

module Chocolate
  module Style
  end
end

require 'chocolate/style/breadcrumbs'
require 'chocolate/style/core_ext'
require 'chocolate/style/helpers'
