module Chocolate
  module Style
    class Engine < ::Rails::Engine
      
      # Chocolate configuration
      config.chocolate = ActiveSupport::OrderedOptions.new
      config.chocolate.title = "Website"
    end
  end
end
