require 'breadcrumbs_on_rails'

module Chocolate
  module Style
    module Breadcrumbs
      
      class Builder < BreadcrumbsOnRails::Breadcrumbs::Builder
        
        def render
          return '' if @elements.empty?
          
          separator = @options[:separator] || " // "
          last      = @elements.pop
          links     = @elements.collect {|e| render_element(e)}
          
          links.push @context.content_tag :span, compute_name(last), class: 'last'
          links.join separator
        end
        
        def render_element(element)
          if element.path == nil
            content = compute_name(element)
          else
            content = @context.link_to_unless_current(compute_name(element), compute_path(element), element.options)
          end
          if @options[:tag]
            @context.content_tag(@options[:tag], content)
          else
            content
          end
        end
  
      end
    end
  end
end