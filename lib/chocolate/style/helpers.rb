require 'chocolate/style/helpers/tags/pseudo_check_box'
require 'chocolate/style/helpers/tags/radio_toggle'
require 'chocolate/style/helpers/tags/slide_toggle'
require 'chocolate/style/helpers/activity_helper'
require 'chocolate/style/helpers/devise_helper'
require 'chocolate/style/helpers/form_helper'
require 'chocolate/style/helpers/full_form_helper'
require 'chocolate/style/helpers/sly_helper'
require 'chocolate/style/helpers/tag_helper'
require 'chocolate/style/helpers/text_helper'


module Chocolate
  module Style
    module Helpers
      extend ActiveSupport::Concern
      
      include TagHelper
      include FormTagHelper
      include FormHelper
      include FullFormHelper
      include SlyHelper
      include DeviseHelper
      include ActivityHelper
          
      included do
        ActionView::Helpers::FormBuilder.send :include, Chocolate::Style::Helpers::FormBuilder
      end
    end
  end
end


ActionView::Base.send :include, Chocolate::Style::Helpers