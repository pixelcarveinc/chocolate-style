module Chocolate
  module Style
    module Helpers
      module TextHelper
        
        # Create plain text short form of text/HTML string with no more than
        # specified max characters.
        def short_form(str, max_chars=70)
          clean = ActionView::Base.full_sanitizer.sanitize(str)
          return clean if clean.length <= max_chars
          
          words = clean.split(' ')
          short = ''
          while short.length < max_chars do
            short += words.shift + ' '
          end
          short.rstrip + '...'
        end
        
      end
    end
  end
end