module Chocolate
  module Style
    module Helpers
      module Tags
        
        # A pseudo form element that appears as a pair of radio buttons for
        # true/false values.
        class RadioToggle < ActionView::Helpers::Tags::CheckBox
  
          def render
            options = @options.stringify_keys
            add_default_name_and_id options if object
            
            checked = input_checked?(object, options)
            
            options['type']  = 'hidden'
            options['value'] = checked ? @checked_value : @unchecked_value
            
            id         = options.delete('id')
            klass      = options.delete 'class'
            klass      = klass ? klass + ' radio-toggle' : 'radio-toggle' 
            on_label   = options.delete('on_label')  || 'On'
            off_label  = options.delete('off_label') || 'Off'
            unselected = 'pseudo-radio'
            selected   = unselected + ' checked'
            
            on         = content_tag :a, on_label,   data: {value: @checked_value}, class: 'on '  + (checked ? selected : unselected)
            off        = content_tag :a, off_label,  data: {value: @unchecked_value}, class: 'off ' + (checked ? unselected : selected)
            on_label   = content_tag :label, on,  class: 'radio-label'
            off_label  = content_tag :label, off, class: 'radio-label'
            hidden     = tag :input, options
            
            content_tag :div, on_label + off_label + hidden, id: id, class: klass
          end
        end
        
      end
    end
  end
end