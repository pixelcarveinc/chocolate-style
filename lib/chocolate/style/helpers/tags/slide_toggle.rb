module Chocolate
  module Style
    module Helpers
      module Tags
        
        class SlideToggle < ActionView::Helpers::Tags::CheckBox
  
          def render
            options = @options.stringify_keys
            add_default_name_and_id options
            
            checked = input_checked?(object, options)
            
            options['type']  = 'hidden'
            options['value'] = checked ? @checked_value : @unchecked_value
            
            id    = options.delete 'id'
            klass = options.delete 'class'
            klass = klass ? klass + ' slide-toggle' : 'slide-toggle'
  
            handle = content_tag :div, '', class: 'handle'
            on     = content_tag :div, '', data: {value: @checked_value},   class: 'on-state'
            off    = content_tag :div, '', data: {value: @unchecked_value}, class: 'off-state'
            slide  = content_tag :div, handle + on + off, class: 'slide ' + (checked ? 'on' : 'off')
            hidden = tag :input, options
            
            content_tag :div, slide + hidden, id: id, class: klass
          end
        end
      end
    end
  end
end