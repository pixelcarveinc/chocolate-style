module Chocolate
  module Style
    module Helpers
      module Tags
        
        # A pseudo form element that appears as a styled checkbox.
        class PseudoCheckBox < ActionView::Helpers::Tags::CheckBox
  
          def render
            options = @options.stringify_keys
            add_default_name_and_id options
            
            checked = input_checked?(object, options)
            
            options['type']   = 'hidden'
            options['value']  = checked ? @checked_value : @unchecked_value
            options['data'] ||= {}
            options['data']['checked_value']   = @checked_value
            options['data']['unchecked_value'] = @unchecked_value
            
            id    = options.delete 'id'
            klass = options.delete 'class'
            klass = klass ? klass + ' checkbox' : 'checkbox'
            
            button = button_tag '', class: 'pseudo-checkbox' + (checked ? ' checked' : '')
            hidden = tag :input, options
            content_tag :div, button + hidden, id: id, class: klass
          end
        end
        
      end
    end
  end
end