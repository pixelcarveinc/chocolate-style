module Chocolate
  module Style
    module Helpers
      
      module FormTagHelper
        
        def radio_toggle_tag(name, value = '1', checked = false, options = {})
          options['checked'] = checked
          options['name']    = name
          options['id']      = sanitize_to_id(name)
          Tags::RadioToggle.new(nil, name, self, value, '0', options).render
        end
        
        def slide_toggle_tag(name, value = '1', checked = false, options = {})
          options['checked'] = checked
          options['name']    = name
          options['id']      = sanitize_to_id(name)
          Tags::SlideToggle.new(nil, name, self, value, '0', options).render
        end
        
        def pseudo_check_box_tag(name, value = '1', checked = false, options = {})
          options['checked'] = checked
          options['name']    = name
          options['id']      = sanitize_to_id(name)
          Tags::PseudoCheckBox.new(nil, name, self, value, '0', options).render
        end
      end
      
      
      module FormHelper
        
        def radio_toggle(object_name, method, options = {}, checked_value = "1", unchecked_value = "0")
          Tags::RadioToggle.new(object_name, method, self, checked_value, unchecked_value, options).render
        end
        
        def slide_toggle(object_name, method, options = {}, checked_value = "1", unchecked_value = "0")
          Tags::SlideToggle.new(object_name, method, self, checked_value, unchecked_value, options).render
        end
        
        def pseudo_check_box(object_name, method, options = {}, checked_value = "1", unchecked_value = "0")
          Tags::PseudoCheckBox.new(object_name, method, self, checked_value, unchecked_value, options).render
        end
      end
      
      
      module FormBuilder
        
        def radio_toggle(method, options = {}, checked_value = "1", unchecked_value = "0")
          @template.radio_toggle @object_name, method, objectify_options(options), checked_value, unchecked_value
        end
        
        def slide_toggle(method, options = {}, checked_value = "1", unchecked_value = "0")
          @template.slide_toggle @object_name, method, objectify_options(options), checked_value, unchecked_value
        end
        
        def pseudo_check_box(method, options = {}, checked_value = "1", unchecked_value = "0")
          @template.pseudo_check_box @object_name, method, objectify_options(options), checked_value, unchecked_value
        end
      end
    end
  end
end
