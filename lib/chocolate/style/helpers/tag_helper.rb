module Chocolate
  module Style
    module Helpers
      module TagHelper
        
        # Render components for a sly scroller.
        def sly_tag
          concat(content_tag(:div, class: 'scrollbar') do
            content_tag(:div, class: 'handle') do
              content_tag :div, '', class: 'mousearea'
            end
          end)
          concat(content_tag(:div, class: 'frame') { capture { yield } })
          nil
        end
  
        # Output all flash messages in a #flash div.
        def flash_messages
          if flash.any?
            content_tag :div, id: 'flash' do
              flash.map do |name, msg|
                concat(content_tag(:p, msg)) if msg.is_a? String
              end
            end
          end
        end
        
      end
    end
  end
end