module Chocolate
  module Style
    module Helpers
      module FullFormHelper

        # Render a full form (takes up full height and is scrollable.)
        def full_form_for(record_or_name_or_array, *args, &block)
          options = args.extract_options!

          options[:html] ||= {}
          options[:html][:class] = options[:html][:class].nil? ? 'full' : options[:html][:class] + ' full'

          form_for(record_or_name_or_array, *(args << options.merge(builder: FullFormBuilder)), &block)
        end


        # Custom form builder for full forms.
        class FullFormBuilder < ActionView::Helpers::FormBuilder
          include Chocolate::Style::Helpers::FormBuilder

          def fields
            @template.capture do
              @template.sly_tag do
                @template.content_tag(:fieldset, class: 'fields') { yield }
              end
            end
          end

          def row
            @template.content_tag(:div, class: 'formrow') do
              # get block's content, not return value
              @template.concat(@template.capture { yield })
              @template.concat @template.content_tag(:div, '', class: 'cll')
            end
          end

          # Render controls fieldset
          def controls(back_path = nil)
            @template.content_tag(:fieldset, id: 'controls') do
              unless back_path.nil?
                @template.concat @template.link_to('Back', back_path, class: 'left back')
              end
              @template.concat self.submit('Save', class: 'right save')
            end
          end

          def wrap_item(method, options = {})
            text = options[:label]

            html_class  = 'item'
            html_class << ' full'  if options[:full]
            html_class << ' error' if object.errors.include? method

            @template.content_tag(:div, class: html_class) do
              @template.concat self.label(method, text)
              @template.concat(@template.capture { yield })

              if object.errors.include? method
                @template.concat @template.content_tag(:div, object.errors[method].join(', '), class: 'error-message')
              end
            end
          end

          def date_field(method, options = {})
            options[:class] ||= ''
            options[:class].concat(' datetimepicker').rstrip!
            options[:value] = object[method].to_s(:na) unless options.key? :value

            self.text_field(method, options)
          end

          def image_field(method, options = {})
            image = object.send method.to_sym
            thumb = options[:thumb]
            thumb = image.url if thumb.nil? && image.file?
            large = options[:large]
            large = image.url if large.nil? && image.file?

            self.wrap_item(method, options) do
              @template.content_tag(:div, class: 'image-field') do
                unless thumb.blank?
                  @template.concat(@template.link_to(large, data: { lightbox: 'file' }) do
                    @template.image_tag(thumb)
                  end)
                end
                @template.concat(@template.content_tag(:div, class: 'file-select') do
                  @template.concat(self._file_field(method, accept: 'image/png,image/jpeg,image/pjpeg'))
                  @template.concat('Select')
                end)
                @template.concat(@template.content_tag(:div, '', class: 'selected'))

                
                if object.send(method.to_sym).exists?

                  @template.concat(@template.content_tag(:div, '', class: 'delete') do
                    @template.concat(self.check_box('delete_' + method.to_s))
                    @template.concat(@template.content_tag(:span, 'Delete', class: 'delete_label'))
                  end)

                end

      # = f.check_box :delete_model_photo
      # = f.label :delete_model_photo, "Delete?"
              end
            end
          end

          def upload_field(method, options = {})

            file = object.send method.to_sym

            self.wrap_item(method, options) do

              @template.content_tag(:div, class: 'image-field') do
                
                if file.url.present?
                  @template.concat(@template.link_to(file.url, {}) do
                    @template.content_tag(:i, '', class: 'icon-doc')
                  end)
                end


                @template.concat(@template.content_tag(:div, class: 'file-select') do

                  if options[:accept].present?
                    @template.concat(self._file_field(method, accept: options[:accept]))
                  else
                    @template.concat(self._file_field(method, accept: 'application/octet-stream'))
                  end

                  @template.concat('Select')
                end)
                @template.concat(@template.content_tag(:div, '', class: 'selected'))

                
                if object.send(method.to_sym).exists?

                  @template.concat(@template.content_tag(:div, '', class: 'delete') do
                    @template.concat(self.check_box('delete_' + method.to_s))
                    @template.concat(@template.content_tag(:span, 'Delete', class: 'delete_label'))
                  end)

                end


              end


            end


          end


          FIELDS = %w(text_field file_field text_area datetime_select password_field)

          FIELDS.each do |method|
            
            class_eval <<-RUBY_EVAL, __FILE__, __LINE__ + 1
              alias :_#{method} :#{method}

              def #{method}(method, options = {})
                wrap_item(method, options) { self._#{method}(method, options) }
              end
            RUBY_EVAL
          end

          alias _radio_toggle radio_toggle
          def radio_toggle(method, options = {}, checked_value = "1", unchecked_value = "0")
            wrap_item(method, options) { self._radio_toggle method, options, checked_value, unchecked_value }
          end

          alias _slide_toggle slide_toggle
          def slide_toggle(method, options = {}, checked_value = "1", unchecked_value = "0")
            wrap_item(method, options) { self._slide_toggle method, options, checked_value, unchecked_value }
          end

          alias _pseudo_check_box pseudo_check_box
          def pseudo_check_box(method, options = {}, checked_value = "1", unchecked_value = "0")
            wrap_item(method, options) { self._pseudo_check_box method, options, checked_value, unchecked_value }
          end

          alias _select select
          def select(method, choices, options = {}, html_options = {})
            wrap_item(method, options) { self._select method, choices, options, html_options }
          end

        end

      end
    end
  end
end
