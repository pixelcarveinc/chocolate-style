module Chocolate
  module Style
    module Helpers
      
      module SlyHelper
        
        # Render components for a sly scroller
        def sly
          concat(content_tag(:div, class: 'scrollbar') do
            content_tag(:div, class: 'handle') do
              content_tag :div, '', class: 'mousearea'
            end
          end)
          concat(content_tag(:div, class: 'frame') { capture { yield } })
          nil
        end
        
      end
    end
  end  
end