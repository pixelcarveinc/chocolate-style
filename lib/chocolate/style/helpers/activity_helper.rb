module Chocolate
  module Style
    module Helpers
      module ActivityHelper
  
        def activity_title(activity)
          title = content_tag :strong, activity.parameters[:title] 
          "\"#{title}\"".html_safe
        end
        
        def activity_owner(activity)
          content_tag :strong, activity.owner == current_user ? 'You' : activity.owner.name
        end
      
        def activity_recipient(activity)
          if activity.recipient
            name = activity.recipient == current_user ? 'you' : activity.recipient.name
            recipient = content_tag :strong, name
            "for #{recipient}".html_safe
          end
        end
        
      end
    end
  end
end
      