module Chocolate
  module Style
    module Helpers
      
      module DeviseHelper

        # Render errors from devise forms
        def devise_form_errors!
          if resource.errors.any?
            content_tag(:ul, id: 'form_errors') do
              resource.errors.full_messages.each do |error|
                concat content_tag :li, error
              end
            end
          end
        end
        
      end
    end
  end  
end